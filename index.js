let valores = [
  " ",
  "um",
  "dois",
  "tres",
  "quatro",
  "cinco",
  "seis",
  "sete",
  "oito",
  "nove",
];
let valoresDezmais = [
  "dez",
  "onze",
  "doze",
  "treze",
  "quatorze",
  "quinze",
  "dezesseis",
  "dezessete",
  "dezoito",
  "dezenove",
];
let valoresDezenas = [
  "vinte",
  "trinta",
  "quarenta",
  "cinquenta",
  "sessenta",
  "setenta",
  "oitenta",
  "noventa",
];
let valoresCentenas = [
  "cento",
  "duzentos",
  "trezentos",
  "quatrocentos",
  "quinhentos",
  "seiscentos",
  "setecentos",
  "oitocentos",
  "novecentos",
  "mil",
];
function numbersToWords() {
  result = [];
  for (let i = 0; i < valores.length; i++) {
    result.push(valores[i]);
  }
  for (let j = 0; j < valoresDezmais.length; j++) {
    result.push(valoresDezmais[j]);
  }
  for (let z = 0; z < valoresDezenas.length; z++) {
    for (let y = 0; y < valores.length; y++) {
      if (y > 0) {
        result.push(`${valoresDezenas[z]} e ${valores[y]}`);
      } else {
        result.push(`${valoresDezenas[z]}`);
      }
    }
  }
  for (let w = 0; w < valoresCentenas.length; w++) {
    for (let a = 0; a < valores.length; a++) {
      if (a > 0) {
        result.push(`${valoresCentenas[w]} e ${valores[a]}`);
      } else {
        result.push(`${valoresCentenas[w]}`);
      }
    }
    for (let b = 0; b < valores.length; b++) {
      result.push(`${valoresCentenas[w]} e ${valoresDezmais[b]}`);
    }
    for (let c = 0; c < valoresDezenas.length; c++) {
      for (let d = 0; d < valores.length; d++) {
        if (d > 0) {
          result.push(
            `${valoresCentenas[w]} e ${valoresDezenas[c]} e ${valores[d]}`
          );
        } else {
          result.push(`${valoresCentenas[w]} e ${valoresDezenas[c]}`);
        }
      }
    }
    if (result[100]) {
      result[100] = "cem";
    }
  }
  console.log(result[100]);
}

function showResult(numero) {
  numbersToWords();
  const resultado = document.getElementById("resposta");
  resultado.innerText = `${result[numero]}`;
}

const button = document.getElementById("button");
button.addEventListener("click", function () {
  const input = document.getElementById("numero").value;
  showResult(input);
});
